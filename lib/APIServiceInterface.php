<?php
/**
 * @file
 */

namespace Drupal\cakemail_api;

/**
 * Interface for CakeMail Service API.
 *
 * Instances of this interfaces should support methods invocation for the
 * methods supported by the matching service.
 */
interface APIServiceInterface {

}
